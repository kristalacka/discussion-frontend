import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import AccountCircle from '@mui/icons-material/AccountCircle';
import './Navbar.css'
import { useNavigate } from 'react-router-dom';
import { useState } from 'react'
import Drawer from '@material-ui/core/Drawer';
import CloseIcon from '@material-ui/icons/Close';
import Divider from '@material-ui/core/Divider';
import { useTheme } from '@mui/material/styles';
import AuthService from '../../services/authService'


const pages = {
    allPosts: { name: 'All posts', route: '/' },
    subs: { name: 'Sub list', route: '/subs' }
}
const settingsLoggedIn = {
    profile: { name: 'Profile', route: '/profile' },
    logout: { name: 'Log out', route: '/' },
};
const settingsLoggedOut = {
    login: { name: 'Log in', route: '/login' },
    signup: { name: 'Sign up', route: '/register' }
};

const ResponsiveAppBar = () => {
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const navigate = useNavigate();

    const handleProfileMenuClicked = (collection, key) => {
        navigate(collection[key].route);
        handleCloseUserMenu();
        if (key === "logout") {
            AuthService.logout();
            window.location.reload(false);
        }
    }

    const [drawerOpen, setDrawerState] = useState(false);
    const toggleDrawer = (drawerOpen) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setDrawerState(drawerOpen);
    };

    const theme = useTheme();

    return (
        <AppBar position="fixed" style={{ background: theme.palette.primary.main }} sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    {/* Hamburger */}
                    <IconButton edge="start" color="inherit" aria-label="open drawer" onClick={toggleDrawer(true)}
                        sx={{ mr: 2, display: { xs: 'block', md: 'none', }, }}>
                        <MenuIcon />
                    </IconButton>
                    <Drawer
                        anchor="left"
                        open={drawerOpen}
                    >
                        {/* The inside of the drawer */}
                        <Box sx={{
                            p: 2,
                            height: 1
                        }}>
                            <IconButton onClick={toggleDrawer(false)} sx={{ mb: 2 }}>
                                <CloseIcon />
                            </IconButton>

                            <Divider sx={{ mb: 2 }} />

                            {Object.keys(pages).map((page) => (
                                <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                                    <Button
                                        key={page}
                                        onClick={() => { setDrawerState(false); navigate(pages[page].route); }}
                                        sx={{ display: 'block' }}
                                    >
                                        {pages[page].name}
                                    </Button>
                                </Box>
                            ))}
                        </Box>
                    </Drawer>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
                    >
                        <img src="/discussion-logo.svg" className={`mobile-icon`} alt="Logo" style={{ height: 50 }} />
                    </Typography>

                    {/* Desktop view */}
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                    >
                        <img src="/discussion-logo.svg" className="svg-filter" alt="Logo" style={{ height: 50, paddingRight: 20 }} />
                    </Typography>
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {Object.keys(pages).map((page) => (
                            <Button
                                key={page}
                                onClick={() => { navigate(pages[page].route) }}
                                sx={{ my: 2, color: 'white', display: 'block' }}
                            >
                                {pages[page].name}
                            </Button>
                        ))}
                    </Box>

                    <Box sx={{ flexGrow: 0 }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleUserMenu}
                            style={{ color: theme.palette.primary.contrastText }}
                        >
                            <AccountCircle />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {AuthService.getCurrentUser() ? Object.keys(settingsLoggedIn).map((setting) => (
                                <MenuItem key={setting} onClick={() => handleProfileMenuClicked(settingsLoggedIn, setting)}>
                                    <Typography textAlign="center">
                                        {settingsLoggedIn[setting].name}
                                    </Typography>
                                </MenuItem>
                            )) : Object.keys(settingsLoggedOut).map((setting) => (
                                <MenuItem key={setting} onClick={() => handleProfileMenuClicked(settingsLoggedOut, setting)}>
                                    <Typography textAlign="center">
                                        {settingsLoggedOut[setting].name}
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar >
    );
};
export default ResponsiveAppBar;
