import React, { useState } from 'react'
import { useNavigate, Navigate } from 'react-router-dom';

import './Login.css'

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import AuthService from '../../services/authService'

export default function Login() {
    const navigate = useNavigate();

    const [usernameError, setUsernameError] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [loginError, setLoginError] = useState("");

    if (AuthService.getCurrentUser()) {
        return <Navigate to='/' />;
    }

    const handleSubmit = async e => {

        e.preventDefault();
        const data = new FormData(e.currentTarget);
        if (!data.get("username")) {
            setUsernameError("This field is required");
            return
        }
        if (!data.get("password")) {
            setPasswordError("This field is required");
            return
        }
        setUsernameError("")
        setPasswordError("")
        setLoginError("")

        AuthService.login(data.get("username"), data.get("password"))
            .then(() => {
                navigate('/');
            })
            .catch(error => {
                setLoginError(error.response.data.error);
            });
    }
    return (
        <div className="login-wrapper">
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                {loginError !== "" && <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {loginError}
                </Alert>}
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="Username"
                    name="username"
                    error={usernameError !== ""}
                    autoComplete="username"
                    autoFocus
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    error={passwordError !== ""}
                    autoComplete="current-password"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Sign In
                </Button>
            </Box>
        </div>
    )
}