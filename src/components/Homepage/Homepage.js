import React, { useState, useEffect } from 'react'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import AccessibleIcon from '@mui/icons-material/Accessible';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { Divider, Link, Switch, Typography } from '@mui/material';
import DataService from '../../services/dataService'
import AuthService from '../../services/authService'
import TokenService from 'services/tokenService';
import HelperService from 'services/helperService';


export default function Homepage({ partyMode, setPartyMode, friends, setFriends }) {
    const [posts, setPosts] = useState([]);

    const fetchPosts = async () => {
        await DataService.getAllPosts()
            .then((data => data.data))
            .then(function (data) {
                setPosts(data);
            }).catch(() => {
                TokenService.removeUser();
                fetchPosts(); // works?
            })
    };

    useEffect(() => {
        fetchPosts();
    }, []);

    const handleVote = (post, index, vote) => {
        var finalVote = vote;
        if (post.vote === vote) {
            finalVote = 0;
        }

        DataService.votePost(post, finalVote).then(function () {
            var postsCopy = JSON.parse(JSON.stringify(posts));
            var relativeVote = 0;
            if (post.vote === -1) {
                if (vote === -1) {
                    relativeVote = 1;
                }
                else {
                    relativeVote = 2;
                }
            } else if (post.vote === 1) {
                if (vote === -1) {
                    relativeVote = -2;
                }
                else {
                    relativeVote = -1;
                }
            } else {
                relativeVote = vote;
            }
            postsCopy[index].score = postsCopy[index].score + relativeVote;
            postsCopy[index].vote = finalVote;
            setPosts(postsCopy);
        })
    }

    return (
        <>
            Party mode
            <Switch checked={partyMode} onChange={(event) => { setPartyMode(event.target.checked) }} color="secondary" />
            Extra based mode
            <Switch disabled={partyMode ? false : true} checked={friends} onChange={(event) => { setFriends(event.target.checked) }} color="error" />
            <h2>These are the top posts of the day</h2>
            <List>
                {
                    posts.map((post, index) => (
                        <>
                            <ListItem
                                key={post.id + "-" + post.score}
                                item={post}
                            >
                                <Typography style={{ paddingRight: 20 }}>
                                    {index + 1}
                                </Typography>
                                <ButtonGroup
                                    orientation="vertical"
                                    style={{ paddingRight: 20 }}
                                    variant="text"
                                >
                                    <Button
                                        key={"upvote" + post.id}
                                        disabled={AuthService.getCurrentUser() ? false : true}
                                        color={post.vote === 1 ? 'secondary' : 'primary'}
                                        style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}
                                        onClick={() => {
                                            handleVote(post, index, 1);
                                        }}
                                    >
                                        <KeyboardArrowUpIcon />
                                    </Button>
                                    <Button
                                        key={"score" + post.id}
                                        disabled={true}
                                        className='vote-button'
                                        style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}>
                                        <Typography>
                                            {post.score}
                                        </Typography>
                                    </Button>
                                    <Button
                                        key={"downvote" + post.id}
                                        disabled={AuthService.getCurrentUser() ? false : true}
                                        color={post.vote === -1 ? 'error' : 'primary'}
                                        style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}
                                        onClick={() => {
                                            handleVote(post, index, -1);
                                        }}
                                    >
                                        <KeyboardArrowDownIcon />
                                    </Button>
                                </ButtonGroup>
                                <ListItemAvatar>
                                    <Avatar>
                                        <AccessibleIcon />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={<Link href={`/subs/${post.sub_title}/posts/${post.id}`}>{post.title}</Link>}
                                    secondary={post.text.length > 30 ? post.text.substring(0, 30) + "..." : post.text} />
                                <Typography variant='subtitle2'>
                                    in <strong><Link href={`/subs/${post.sub_title}`}>{post.sub_title}</Link> </strong>
                                    by <strong>{post.creator_username ? post.creator_username : "[unknown]"}</strong> {HelperService.parseDate(post.date_created)} {post.last_edit ? "*(last edited " + HelperService.parseDate(post.last_edit) + ")" : ""}
                                </Typography>
                            </ListItem>
                            <Divider />
                        </>
                    ))
                }
            </List >
        </>
    )
}