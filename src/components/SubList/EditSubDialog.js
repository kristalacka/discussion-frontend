import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useState } from 'react';
import DataService from 'services/dataService';

export default function CreateSubDialog({ setOpen, open, subs, setSubs, sub, setSub }) {
    const handleClose = () => {
        setOpen(false);
    };

    const handleUpdate = async () => {
        DataService.editSub(sub.id, { description: subDescription })
            .then(() => {
                handleClose();

                var newRows = JSON.parse(JSON.stringify(subs));
                for (var i = 0; i < subs.length; i++) {
                    if (subs[i].id === sub.id) {
                        newRows[i].description = subDescription;
                        setSubs(newRows);
                        break;
                    }
                }
            })
            .catch((e) => {
                console.log(e);
            });
        setSub(null);
    }

    const [subDescription, setSubDescription] = useState("");

    return (
        <div>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Edit sub </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        You may edit the sub's description
                    </DialogContentText>
                    <TextField
                        margin="normal"
                        fullWidth
                        name="description"
                        label="Description"
                        id="description"
                        defaultValue={sub ? sub.description : ""}
                        multiline
                        rows={4}
                        onChange={(e) => setSubDescription(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleUpdate}>Update</Button>
                </DialogActions>

            </Dialog>
        </div>
    );
}
