import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Divider } from '@mui/material';
import { useState } from 'react';
import DataService from 'services/dataService';
import ErrorFormattingService from 'services/errorFormattingService';

export default function CreateSubDialog({ setOpen, open, setAlertSeverity, setAlertMessage, setAlertOpen, subs, setSubs }) {
    const handleClose = () => {
        setOpen(false);
    };

    const handleCreate = async e => {
        if (subTitle.indexOf(' ') >= 0) {
            setAlertSeverity("error");
            setAlertMessage(`Sub name should not contain any spaces`);
            setAlertOpen(true);
            return
        }
        setAlertOpen(false);
        DataService.createSub(subTitle, subDescription)
            .then((data) => {
                handleClose();
                setAlertSeverity("success");
                setAlertMessage(`Successfully created ${subTitle}`);
                setAlertOpen(true);

                var newRows = JSON.parse(JSON.stringify(subs));
                data.data.id = data.data.title;
                newRows.push(data.data);
                setSubs(newRows);
            })
            .catch((e) => {
                setAlertSeverity("error");
                setAlertMessage(`Could not create ${subTitle}: ${ErrorFormattingService.format(e)}`);
                setAlertOpen(true);
            });
    }

    const [subTitle, setSubTitle] = useState("");
    const [subDescription, setSubDescription] = useState("");

    return (
        <div>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Create new sub</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To create a new sub, give it a name and a brief description
                    </DialogContentText>
                    <TextField
                        required
                        autoFocus
                        margin="dense"
                        id="title"
                        label="Sub name"
                        fullWidth
                        variant="standard"
                        onChange={(e) => setSubTitle(e.target.value)}
                    />
                    <Divider style={{ padding: 10 }} />
                    <TextField
                        margin="normal"
                        fullWidth
                        name="description"
                        label="Description"
                        id="description"
                        multiline
                        rows={4}
                        onChange={(e) => setSubDescription(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleCreate}>Create</Button>
                </DialogActions>

            </Dialog>
        </div>
    );
}
