import React, { useState, useEffect } from 'react'
import DataService from 'services/dataService';
import AuthService from 'services/authService';
import { DataGrid, GridActionsCellItem } from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import { Box, Link } from '@mui/material';
import CreateSubDialog from './CreateSubDialog';
import PopupAlert from 'components/PopupAlert';
import ConfirmationDialog from '../ConfirmationDialog';
import ErrorFormattingService from 'services/errorFormattingService';
import EditSubDialog from './EditSubDialog';

export default function SubList() {
    const [subs, setSubs] = useState([]);

    const fetchSubs = async () => {
        await DataService.getAllSubs()
            .then((data => data.data))
            .then(function (data) {
                for (let index = 0; index < data.length; index++) {
                    console.log(data[index])
                    data[index].id = data[index].title
                }
                console.log(data)
                setSubs(data);
            })
    };

    useEffect(() => {
        fetchSubs();
    }, []);

    const columns = [
        {
            field: 'title',
            headerName: 'Sub title',
            flex: 1,
            renderCell: (params) => (
                <Link href={`subs/${params.value}`}>{params.value}</Link>
            )
        },
        {
            field: 'members',
            headerName: 'Members',
            type: 'number',
            flex: 1
        },
        { field: 'description', headerName: 'Description', sortable: false, flex: 1 },
        // { field: 'creator', headerName: 'Creator', flex: 1 },
        // { field: 'subscribed', type: 'boolean', headerName: 'Subscribed', flex: 1 },
    ];

    const currentUser = AuthService.getCurrentUser()
    if (currentUser) {
        columns.push(
            {
                field: 'subscribe-action',
                type: 'actions',
                headerName: 'Subscribe',
                flex: 1,
                // @ts-ignore
                getActions: (params) => {
                    if (!params.row.subscribed) {
                        return [
                            <GridActionsCellItem
                                icon={<AddCircleIcon />}
                                label="Subscribe"
                                // @ts-ignore
                                onClick={() => { handleSubscribe(params) }}
                            />
                        ]
                    }
                    return [<GridActionsCellItem
                        icon={<RemoveCircleIcon />}
                        label="Unsubscribe"
                        // @ts-ignore
                        onClick={() => { handleUnsubscribe(params) }}
                    />]
                }
            },
            {
                field: 'manager-actions',
                type: 'actions',
                flex: 1,
                // @ts-ignore
                getActions: (params) => {
                    var actions = []
                    if (currentUser.is_admin || currentUser.user_id === params.row.creator) {
                        actions.push(
                            <GridActionsCellItem
                                icon={<DeleteIcon />}
                                label="Delete"
                                onClick={() => { handleClickDelete(params) }}
                            />,
                            <GridActionsCellItem
                                icon={<EditIcon />}
                                label="Edit"
                                // @ts-ignore
                                onClick={() => { handleClickEdit(params) }}
                            />
                        )
                    }

                    return actions
                },
            }
        )
    }

    const getRowIndex = (id) => {
        var index = subs.findIndex(function (o) {
            return o.id === id;
        })
        return index
    }

    const removeRow = (id) => {
        var newRows = JSON.parse(JSON.stringify(subs));
        var index = getRowIndex(id)
        if (index !== -1) {
            newRows.splice(index, 1);
            setSubs(newRows);
        }
    }

    const replaceRow = (id, row) => {
        var newRows = JSON.parse(JSON.stringify(subs));
        var index = getRowIndex(id)
        if (index !== -1) {
            newRows[index] = row;
            setSubs(newRows);
        }
    }

    const handleClickEdit = (params) => {
        setEditSub(params.row);
        setEditOpen(true);
    }

    const handleClickDelete = (params) => {
        setToDelete(params.row);
        var message = {
            primary: `Are you sure you want to remove sub ${params.row.title}`,
            secondary: "This will remove all associated posts and comments"
        };
        setConfirmationMessage(message);
        setConfirmationOpen(true);
    }

    const handleConfirmDelete = () => {
        DataService.deleteSub(toDelete.title)
            .then(() => {
                removeRow(toDelete.id);
                setAlertSeverity("success");
                setAlertMessage(`Successfully deleted sub ${toDelete.title}`);
                setAlertOpen(true);
                setConfirmationOpen(false);
            })
            .catch((error) => {
                setAlertSeverity("error");
                setAlertMessage(`Could not delete deleted sub ${toDelete.title}: ${ErrorFormattingService.format(error)}`);
                setAlertOpen(true);
            })
        setToDelete(null);
    }

    const handleSubscribe = (params) => {
        DataService.subscribeSub(params.row.id)
            .then(() => {
                var subscribedSub = params.row;
                subscribedSub.subscribed = true;
                subscribedSub.members += 1;
                replaceRow(params.row.id, subscribedSub);
                setAlertSeverity("success");
                setAlertMessage(`You are now subscribed to ${params.row.title}`);
                setAlertOpen(true);
            })
            .catch(() => {
                setAlertSeverity("error");
                setAlertMessage(`Could not subscribe to sub ${params.row.title}`);
                setAlertOpen(true);
            })
    }

    const handleUnsubscribe = (params) => {
        DataService.unsubscribeSub(params.row.id)
            .then(() => {
                var unsubscribedSub = params.row;
                unsubscribedSub.subscribed = false;
                unsubscribedSub.members -= 1;
                replaceRow(params.row.id, unsubscribedSub);
                setAlertSeverity("success");
                setAlertMessage(`You are now unsubscribed from ${params.row.title}`);
                setAlertOpen(true);
            })
            .catch(() => {
                setAlertSeverity("error");
                setAlertMessage(`Could not unsubscribe from ${params.row.title}`);
                setAlertOpen(true);
            })
    }

    const [sortModel, setSortModel] = React.useState([
        {
            field: 'members',
            sort: 'desc',
        },
    ]);

    const [createOpen, setCreateOpen] = React.useState(false);
    const [editOpen, setEditOpen] = React.useState(false);

    // const [open, setCreateOpen] = React.useState(false);

    const handleCreateOpen = () => {
        setCreateOpen(true);
    };

    const [alertOpen, setAlertOpen] = React.useState(false);
    const [alertSeverity, setAlertSeverity] = React.useState("info");
    const [alertMessage, setAlertMessage] = React.useState("");

    const [confirmationOpen, setConfirmationOpen] = React.useState(false);
    const [confirmationMessage, setConfirmationMessage] = React.useState(false);

    const [toDelete, setToDelete] = React.useState(null);
    const [editSub, setEditSub] = React.useState(null);

    return (
        <>
            <Box style={{ display: 'flex', flexDirection: "column", height: "50vh" }}>
                {AuthService.getCurrentUser() && <Button
                    variant="outlined"
                    color="primary"
                    style={{ maxWidth: "20%", display: 'flex', minWidth: 100 }}
                    onClick={handleCreateOpen}
                >
                    <AddIcon style={{ paddingRight: 10 }} /> Create new sub
                </Button>}
                <DataGrid
                    rows={subs}
                    columns={columns}
                    pageSize={10}
                    rowsPerPageOptions={[10]}
                    // @ts-ignore
                    sortModel={sortModel}
                    onSortModelChange={(model) => setSortModel(model)}
                />
            </Box >
            <CreateSubDialog
                setOpen={setCreateOpen}
                open={createOpen}
                setAlertSeverity={setAlertSeverity}
                setAlertMessage={setAlertMessage}
                setAlertOpen={setAlertOpen}
                subs={subs}
                setSubs={setSubs}
            />
            <EditSubDialog
                setOpen={setEditOpen}
                open={editOpen}
                subs={subs}
                setSubs={setSubs}
                sub={editSub}
                setSub={setEditSub}
            />
            <PopupAlert
                severity={alertSeverity}
                message={alertMessage}
                open={alertOpen}
                setOpen={setAlertOpen}
            />
            <ConfirmationDialog
                open={confirmationOpen}
                setOpen={setConfirmationOpen}
                message={confirmationMessage}
                handleConfirm={handleConfirmDelete}
            />
        </>
    )
}
