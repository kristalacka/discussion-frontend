import React, { useState } from 'react'
import { useNavigate, Navigate } from 'react-router-dom';

import './Register.css'

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import AuthService from '../../services/authService'
import ErrorFormattingService from 'services/errorFormattingService';

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

export default function Login() {
    const navigate = useNavigate();

    const [usernameError, setUsernameError] = useState("");
    const [emailError, setEmailError] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [registerError, setRegisterError] = useState("");

    if (AuthService.getCurrentUser()) {
        return <Navigate to='/' />;
    }

    const handleSubmit = async e => {
        e.preventDefault();
        setUsernameError("")
        setEmailError("")
        setPasswordError("")
        setRegisterError("")

        const data = new FormData(e.currentTarget);

        var error = false;
        if (!data.get("username")) {
            setUsernameError("Username field is required");
            error = true;
        }
        if (!data.get("email")) {
            setEmailError("Email field is required");
            error = true;
        }
        if (!validateEmail(data.get("email"))) {
            setEmailError("Invalid email");
            error = true;
        }
        if (!data.get("password")) {
            setPasswordError("Password field is required");
            error = true;
        }
        if (data.get("password").length < 4) {
            setPasswordError("Password must be at least 4 characters");
            error = true;
        }
        if (error) {
            return
        }

        AuthService.register(data.get("username"), data.get("email"), data.get("password"))
            .then(() => {
                navigate('/login');
            })
            .catch(error => {
                setRegisterError(ErrorFormattingService.format(error));
            });
    }
    return (
        <div className="register-wrapper">
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                {
                    registerError !== "" &&
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {registerError}
                    </Alert>
                }
                {
                    usernameError !== "" &&
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {usernameError}
                    </Alert>
                }
                {
                    emailError !== "" &&
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {emailError}
                    </Alert>
                }
                {
                    passwordError !== "" &&
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {passwordError}
                    </Alert>
                }
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="Username"
                    name="username"
                    error={usernameError !== ""}
                    autoComplete="username"
                    autoFocus
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    error={emailError !== ""}
                    autoComplete="email"
                    autoFocus
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    error={passwordError !== ""}
                    autoComplete="new-password"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Register
                </Button>
            </Box>
        </div>
    )
}