import { Box, Button, ButtonGroup, Divider, Grid, Link, ListItem, TextField, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import AuthService from 'services/authService';
import DataService from 'services/dataService';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import HelperService from 'services/helperService';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import ConfirmationDialog from 'components/ConfirmationDialog';


export default function Comment({ comment, level, subName, post, comments, setComments }) {
    const [username, setUsername] = useState("");

    useEffect(() => {
        HelperService.fetchUsername(comment.creator).then((data) => {
            setUsername(data);
        })
    }, [])

    const handleVote = (vote) => {
        var finalVote = vote;
        if (comment.vote === vote) {
            finalVote = 0;
        }

        DataService.voteComment(subName, post.id, comment.id, finalVote).then(function () {
            var relativeVote = 0;
            if (comment.vote === -1) {
                if (vote === -1) {
                    relativeVote = 1;
                }
                else {
                    relativeVote = 2;
                }
            } else if (comment.vote === 1) {
                if (vote === -1) {
                    relativeVote = -2;
                }
                else {
                    relativeVote = -1;
                }
            } else {
                relativeVote = vote;
            }
            var commentsCopy = JSON.parse(JSON.stringify(comments));
            comment.score += relativeVote;
            comment.vote = finalVote;
            commentsCopy = updateComment(commentsCopy, comment);
            setComments(commentsCopy);
        })
    }

    const [replyOpen, setReplyOpen] = useState(false);
    const [insertComment, setInsertComment] = useState(null);
    const [commentBlank, setCommentBlank] = useState(false);

    const makeSpacers = () => {
        var items = [];
        for (let index = 0; index < level; index++) {
            items.push(
                <Grid item xs={1}>
                    <Divider orientation='vertical' />
                </Grid>
            )
        }
        return (
            <>
                {items}
            </>
        );
    }

    const insertChildComment = (commentList, insertedComment) => {
        for (var i = 0; i < commentList.length; i++) {
            if (commentList[i].id === comment.id) {
                commentList[i].children.unshift(insertedComment)
                return commentList;
            }
            insertChildComment(commentList[i].children, insertedComment);
        }
        return commentList;
    }

    const updateComment = (commentList, newComment) => {
        for (var i = 0; i < commentList.length; i++) {
            if (commentList[i].id === comment.id) {
                commentList[i] = newComment;
                return commentList;
            }
            updateComment(commentList[i].children, newComment);
        }
        return commentList;
    }

    const clickSaveComment = () => {
        if (insertComment.length === 0) {
            setCommentBlank(true);
            return
        }
        setCommentBlank(false);
        DataService.createComment(subName, post.id, { text: insertComment, parent: comment.id })
            .then((response) => {
                setReplyOpen(false);
                setInsertComment("");
                var commentsCopy = JSON.parse(JSON.stringify(comments));

                commentsCopy = insertChildComment(commentsCopy, response.data);
                setComments(commentsCopy);
            })
    }

    const [editComment, setEditComment] = useState(false)

    const [confirmationOpen, setConfirmationOpen] = useState(false);
    const confirmationMessage = { primary: "Are you sure you want to delete this comment?", secondary: "The replies will be unaffected" }
    const handleConfirmDelete = () => {
        DataService.editComment(subName, post.id, comment.id, { text: null })
            .then(() => {
                comment.text = null;
                comment.creator = null;

                var commentsCopy = JSON.parse(JSON.stringify(comments));
                updateComment(commentsCopy, comment);
                setComments(commentsCopy);
                setConfirmationOpen(false);
            })
    }

    const [editCommentValue, setEditCommentValue] = useState("");

    const clickEditComment = () => {
        if (editCommentValue === comment.text) {
            setEditComment(false);
            return
        }
        DataService.editComment(subName, post.id, comment.id, { text: editCommentValue })
            .then(() => {
                comment.text = editCommentValue;

                var currentdate = new Date();
                comment.last_edited = currentdate.toLocaleString('en-US')

                var commentsCopy = JSON.parse(JSON.stringify(comments));
                updateComment(commentsCopy, comment);
                setComments(commentsCopy);
                setEditComment(false);
            })
    }

    const currentUser = AuthService.getCurrentUser()
    return (
        <>
            {level > 0 && makeSpacers()}
            <Grid item xs={level > 6 ? 6 : 12 - level}>
                <ListItem>
                    <ButtonGroup
                        orientation="vertical"
                        variant="text"
                    >
                        <Button
                            key={"upvote" + comment.id}
                            disabled={currentUser ? false : true}
                            color={comment.vote === 1 ? 'secondary' : 'primary'}
                            style={{ maxWidth: '30%', maxHeight: '30%', minWidth: '30%', minHeight: '30%' }}
                            onClick={() => {
                                handleVote(1);
                            }}
                        >
                            <KeyboardArrowUpIcon />
                        </Button>
                        <Button
                            key={"downvote" + comment.id}
                            disabled={currentUser ? false : true}
                            color={comment.vote === -1 ? 'error' : 'primary'}
                            style={{ maxWidth: '30%', maxHeight: '30%', minWidth: '30%', minHeight: '30%' }}
                            onClick={() => {
                                handleVote(-1);
                            }}
                        >
                            <KeyboardArrowDownIcon />
                        </Button>
                    </ButtonGroup>
                    <Box>
                        <Typography variant='subtitle2'>
                            by {comment.text ? username : "[deleted]"} <strong>{comment.score} points</strong> {HelperService.parseDate(comment.date_created)} {comment.last_edit ? "*(last edited " + HelperService.parseDate(comment.last_edit) + ")" : ""}
                        </Typography>
                        {editComment ?
                            <>
                                <TextField
                                    fullWidth
                                    id="standard-multiline-flexible"
                                    multiline
                                    defaultValue={comment.text}
                                    onChange={(event) => { setEditCommentValue(event.target.value) }}
                                    variant="standard"
                                    error={commentBlank}
                                />
                                <Button onClick={() => setEditComment(false)}>
                                    Cancel
                                </Button>
                                <Button onClick={() => clickEditComment()}>
                                    Save
                                </Button>
                            </> :
                            <Typography variant='body1'>
                                {comment.text ? comment.text : "[deleted]"}
                            </Typography>
                        }

                        {currentUser && !editComment &&
                            < Typography variant='subtitle2' paddingTop={1}>
                                <Box sx={{ display: 'inline' }} paddingRight={2}>
                                    <Link component="button" onClick={() => { setReplyOpen(true) }}>
                                        reply
                                    </Link>
                                </Box>
                                {comment.text &&
                                    <>
                                        {currentUser.user_id === comment.creator &&
                                            <>
                                                <Button color='primary' startIcon={<EditIcon />} onClick={() => { setEditComment(true) }} style={{ textTransform: 'none', fontSize: 'small' }}>
                                                    edit
                                                </Button>
                                                <Button color='error' startIcon={<DeleteIcon />} onClick={() => { setConfirmationOpen(true) }} style={{
                                                    textTransform: 'none', fontSize: 'small'
                                                }}>
                                                    delete
                                                </Button>
                                            </>
                                        }
                                        {currentUser.is_admin && currentUser.user_id !== comment.creator &&
                                            <>
                                                <Button color='error' startIcon={<DeleteIcon />} onClick={() => { setConfirmationOpen(true) }} style={{
                                                    textTransform: 'none', fontSize: 'small'
                                                }}>
                                                    delete
                                                </Button>
                                            </>
                                        }
                                    </>
                                }
                            </Typography>
                        }
                    </Box>
                </ListItem>
                {replyOpen &&
                    <>
                        <TextField
                            fullWidth
                            id="standard-multiline-flexible"
                            multiline
                            placeholder="Write a comment"
                            value={currentUser ? insertComment : "Log in or sign up to comment"}
                            onChange={(event) => { setInsertComment(event.target.value) }}
                            variant="standard"
                            error={commentBlank}
                        />
                        <Button onClick={() => setReplyOpen(false)}>
                            Cancel
                        </Button>
                        <Button onClick={() => clickSaveComment()}>
                            Save
                        </Button>
                    </>
                }
            </Grid>
            {
                comment.children.map((child, index) => (
                    <Comment comment={child} level={level + 1} subName={subName} post={post} comments={comments} setComments={setComments} />
                ))
            }
            <ConfirmationDialog
                open={confirmationOpen}
                setOpen={setConfirmationOpen}
                message={confirmationMessage}
                handleConfirm={handleConfirmDelete}
            />
        </>
    )
}
