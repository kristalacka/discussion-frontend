import { Box, Button, Grid, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'
import AuthService from 'services/authService';
import DataService from 'services/dataService';
import Comment from './Comment';


export default function CommentList({ comments, setComments, post, subName }) {
    const clickSaveComment = () => {
        if (insertComment.length === 0) {
            setCommentBlank(true);
            return
        }
        setCommentBlank(false);
        DataService.createComment(subName, post.id, { text: insertComment })
            .then((response) => {
                setInsertComment("");
                var commentsCopy = JSON.parse(JSON.stringify(comments));
                commentsCopy.unshift(response.data);
                setComments(commentsCopy);
            })
    }

    const [insertComment, setInsertComment] = useState("")
    const [commentBlank, setCommentBlank] = useState(false)

    return (
        <div>
            <Typography variant='h6' paddingTop={3}>
                Comments
            </Typography>
            <TextField
                fullWidth
                disabled={AuthService.getCurrentUser() ? false : true}
                id="standard-multiline-flexible"
                multiline
                placeholder="Write a comment"
                value={AuthService.getCurrentUser() ? insertComment : "Log in or sign up to comment"}
                onChange={(event) => { setInsertComment(event.target.value) }}
                variant="standard"
                error={commentBlank}
            />
            {AuthService.getCurrentUser() &&
                <Button onClick={() => clickSaveComment()}>
                    Save
                </Button>
            }
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    {comments.map((comment, index) => (
                        <Comment
                            comment={comment}
                            subName={subName}
                            post={post}
                            level={0}
                            comments={comments}
                            setComments={setComments} />
                    ))}
                </Grid>
            </Box >
        </div>
    )
}
