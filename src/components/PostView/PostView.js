import { Avatar, Box, Button, ButtonGroup, Divider, Grid, IconButton, Link, List, ListItem, ListItemAvatar, ListItemText, TextField, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import DataService from 'services/dataService';
import AuthService from 'services/authService';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import AccessibleIcon from '@mui/icons-material/Accessible';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import ConfirmationDialog from '../ConfirmationDialog';
import PopupAlert from 'components/PopupAlert';
import CommentList from './CommentList';
import HelperService from 'services/helperService';

export default function PostView() {
    const { subName, postId } = useParams();

    function sortComments(commentsList) {
        for (var i = 0; i < commentsList.length; i++) {
            commentsList[i].children = sortComments(commentsList[i].children);
        }
        return commentsList.sort((a, b) => (a.score > b.score) ? -1 : 1)
    }

    const fetchPost = async () => {
        await DataService.getPost(subName, postId)
            .then((data => data.data))
            .then(function (data) {
                setOriginalPost(data);
                setPost(data);
                fetchUsername(data.creator);
            })
            .catch((err) => { })

        await DataService.getComments(subName, postId)
            .then((data => data.data))
            .then(function (data) {
                data.sort((a, b) => (a.score > b.score) ? -1 : 1)
                data = sortComments(data)
                setComments(data);
            })
            .catch((err) => {
                console.log(err)
            })
    };

    useEffect(() => {
        fetchPost();
    }, []);

    const [post, setPost] = useState(null);
    const [originalPost, setOriginalPost] = useState(null);
    const [comments, setComments] = useState([]);

    const [editPost, setEditPost] = useState(false);

    // const [error, setError] = useState(true);
    const handleChangePost = (event) => {
        var clone = JSON.parse(JSON.stringify(post));
        clone.text = event.target.value;
        setPost(clone);
    };

    const clickSavePost = () => {
        DataService.editPost(subName, postId, { text: post.text })
            .then(() => { setEditPost(false); })
            .catch((e) => {
                console.log(e)
            })
    }

    const clickCancelPost = () => {
        setPost(originalPost);
        setEditPost(false);
    }

    const [confirmationOpen, setConfirmationOpen] = useState(false);
    const confirmationMessage = {
        primary: "Are you sure you want to delete the post?",
        secondary: "This will remove any associated comments"
    };

    const [alertOpen, setAlertOpen] = React.useState(false);
    const [alertSeverity, setAlertSeverity] = React.useState("info");
    const [alertMessage, setAlertMessage] = React.useState("");

    const navigate = useNavigate();

    const handleConfirmDelete = () => {
        DataService.deletePost(subName, postId)
            .then(() => {
                setConfirmationOpen(false);
                navigate(`/subs/${subName}`);
            })
            .catch((error) => {
                setAlertSeverity("error");
                setAlertMessage(`Could not delete post`);
                setAlertOpen(true);
            })
    }

    const [username, setUsername] = useState("");

    const fetchUsername = (id) => {
        DataService.getUser(id)
            .then((data => data.data))
            .then(function (data) {
                setUsername(data.username);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    return (
        <div>
            <Link href={`/subs/${subName}`}>Back to {subName}</Link>
            <PostHeader
                subName={subName}
                post={post}
                setPost={setPost}
                setEditPost={setEditPost}
                setConfirmationOpen={setConfirmationOpen}
            />
            <Divider />
            <Typography variant='body1'>
                {editPost ? <TextField
                    fullWidth
                    disabled={!editPost}
                    id="standard-multiline-flexible"
                    multiline
                    value={post ? post.text : ""}
                    onChange={handleChangePost}
                    variant="standard"
                    style={{ padding: 20, backgroundColor: 'rgb(250,250,250)', wordWrap: 'break-word' }}
                />
                    :
                    <>
                        <Box style={{ padding: 20, backgroundColor: 'rgb(250,250,250)' }}>
                            {post ? post.text : ""}
                        </Box>
                        <Divider />
                    </>
                }

            </Typography>
            {editPost &&
                <>
                    <Button onClick={() => clickCancelPost()}>
                        Cancel
                    </Button>
                    <Button onClick={() => clickSavePost()}>
                        Save
                    </Button>
                </>
            }
            {post &&
                <Typography variant='subtitle2'>
                    by <strong>{username}</strong> posted {HelperService.parseDate(post.date_created)} {post.last_edit ? "*(last edited " + HelperService.parseDate(post.last_edit) + ")" : ""}
                </Typography>
            }
            <Divider />
            <CommentList
                comments={comments}
                setComments={setComments}
                post={post}
                subName={subName}
            />
            <ConfirmationDialog
                open={confirmationOpen}
                setOpen={setConfirmationOpen}
                message={confirmationMessage}
                handleConfirm={handleConfirmDelete}
            />
            <PopupAlert
                severity={alertSeverity}
                message={alertMessage}
                open={alertOpen}
                setOpen={setAlertOpen}
            />
        </div>
    )
}

function PostHeader({ subName, post, setPost, setEditPost, setConfirmationOpen }) {
    const handleVote = (post, vote) => {
        var finalVote = vote;
        if (post.vote === vote) {
            finalVote = 0;
        }

        var relativeVote = 0;
        DataService.votePost(post, finalVote).then(function () {
            if (post.vote === -1) {
                if (vote === -1) {
                    relativeVote = 1;
                }
                else {
                    relativeVote = 2;
                }
            } else if (post.vote === 1) {
                if (vote === -1) {
                    relativeVote = -2;
                }
                else {
                    relativeVote = -1;
                }
            } else {
                relativeVote = vote;
            }
        })
            .then(() => {
                var postCopy = JSON.parse(JSON.stringify(post));
                postCopy.score = postCopy.score + relativeVote;
                postCopy.vote = finalVote;
                setPost(postCopy);
            })
    }

    if (!post) {
        return (<div></div>)
    }

    return (
        <>
            <List>
                <ListItem secondaryAction={<SecondaryActionControl
                    owner={post.creator}
                    setEditPost={setEditPost}
                    setConfirmationOpen={setConfirmationOpen}
                />}
                >
                    <ButtonGroup
                        orientation="vertical"
                        style={{ paddingRight: 20 }}
                        variant="text"
                    >
                        <Button
                            key={"upvote" + post.id}
                            disabled={AuthService.getCurrentUser() ? false : true}
                            color={post.vote === 1 ? 'secondary' : 'primary'}
                            style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}
                            onClick={() => {
                                handleVote(post, 1);
                            }}
                        >
                            <KeyboardArrowUpIcon />
                        </Button>
                        <Button
                            key={"score" + post.id}
                            disabled={true}
                            className='vote-button'
                            style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}>
                            <Typography>
                                {post.score}
                            </Typography>
                        </Button>
                        <Button
                            key={"downvote" + post.id}
                            disabled={AuthService.getCurrentUser() ? false : true}
                            color={post.vote === -1 ? 'error' : 'primary'}
                            style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}
                            onClick={() => {
                                handleVote(post, -1);
                            }}
                        >
                            <KeyboardArrowDownIcon />
                        </Button>
                    </ButtonGroup>
                    <ListItemAvatar>
                        <Avatar>
                            <AccessibleIcon />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={post.title} />
                </ListItem>
            </List>
        </>)
}

function SecondaryActionControl({ owner, setEditPost, setConfirmationOpen }) {
    const currentUser = AuthService.getCurrentUser();

    const openConfirmationDialog = () => {
        setConfirmationOpen(true);
    }

    if (currentUser && (currentUser.user_id === owner)) {
        return (<Grid container spacing={2}>
            <Grid item xs={5}>
                <IconButton edge="end" aria-label="edit" color='primary' onClick={() => { setEditPost(true) }}>
                    <EditIcon />
                </IconButton>
            </Grid>
            <Grid item xs={5}>
                <IconButton edge="end" aria-label="delete" color='error' onClick={openConfirmationDialog}>
                    <DeleteIcon />
                </IconButton>
            </Grid>
        </Grid>
        )
    }
    else if (currentUser && currentUser.is_admin) {
        return (
            <Grid container spacing={2}>
                <Grid item xs={5}>
                    <IconButton edge="end" aria-label="delete" color='error' onClick={openConfirmationDialog}>
                        <DeleteIcon />
                    </IconButton>
                </Grid>
            </Grid>
        )
    }

    return (<></>)
}
