import * as React from 'react';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import DataService from 'services/dataService';
import InfoIcon from '@mui/icons-material/Info';
import { Button, ButtonGroup, Grid, IconButton } from '@mui/material';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import AccessibleIcon from '@mui/icons-material/Accessible';
import AuthService from '../../services/authService';
import TokenService from 'services/tokenService';
import { Link } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import ConfirmationDialog from 'components/ConfirmationDialog';
import PopupAlert from 'components/PopupAlert';
import AddIcon from '@mui/icons-material/Add';
import CreatePostDialog from './CreatePostDialog';
import HelperService from 'services/helperService';


const drawerWidth = 240;

export default function SubView() {
    const { subName } = useParams();

    const [sub, setSub] = useState();

    const fetchSub = async () => {
        await DataService.getSub(subName)
            .then((data => data.data))
            .then(function (data) {
                setSub(data);
            })
            .catch((err) => { })
    };

    useEffect(() => {
        fetchSub();
    }, []);

    return (
        <>
            {sub ?
                <div>
                    <SubInfoDrawer sub={sub} />
                    <PostList sub={sub} />
                </div> :
                <div>
                    <h2>Sub not found</h2>
                </div>
            }
        </>
    )
}

const parseDate = (dateString) => {
    const date = new Date(Date.parse(dateString))
    return date.toUTCString();
}

function SubInfoDrawer({ sub }) {
    const [creator, setCreator] = useState("")
    const fetchUsername = () => {
        DataService.getUser(sub.creator)
            .then((data => data.data))
            .then(function (data) {
                setCreator(data.username)
            })
            .catch((err) => {
                console.log(err);
                return null;
            })
    }

    useEffect(() => {
        fetchUsername();
    }, []);

    return (
        <>
            <Drawer
                variant="permanent"
                anchor="right"
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    position: 'fixed',
                    [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
                    display: { xs: 'none', md: 'flex' },
                }}
            >
                <Toolbar />
                <Box sx={{ overflow: 'auto' }}>
                    <List>
                        <ListItem key='subTtile'>
                            <InfoIcon style={{ paddingRight: 10 }} color='secondary' />
                            <ListItemText primary={sub.title} />
                        </ListItem>
                        <ListItem >
                            <Typography variant="body1">
                                {sub.description}
                            </Typography>
                        </ListItem>
                    </List>
                    <Divider />

                    <List>
                        <ListItem >
                            <Typography variant="body1">
                                {"Creator: " + creator}
                            </Typography>
                        </ListItem>
                        <ListItem >
                            <Typography variant="body1">
                                {"Date created: " + parseDate(sub.date_created)}
                            </Typography>
                        </ListItem>
                    </List>
                </Box>
            </Drawer>
            <Box sx={{ overflow: 'auto', display: { xs: 'block', md: 'none' } }}>
                <List>
                    <ListItem>
                        <InfoIcon style={{ paddingRight: 10 }} color='secondary' />
                        <Typography variant="h5">
                            {sub.title + '\n'}
                        </Typography>
                    </ListItem>
                    <ListItem>
                        <Typography variant="body1">
                            {sub.description + '\n'}
                        </Typography>
                    </ListItem>
                    <ListItem>
                        <Typography variant="body1">
                            {"Creator: " + creator + '\n'}
                        </Typography>
                    </ListItem>
                    <ListItem>
                        <Typography variant="body1">
                            {"Date created: " + parseDate(sub.date_created) + '\n'}
                        </Typography>
                    </ListItem>
                    <Divider />
                </List>
            </Box>
        </>
    );
}

function PostList({ sub }) {
    const [posts, setPosts] = useState([]);

    const fetchPosts = async () => {
        await DataService.getPostsBySubs(sub.title)
            .then((data => data.data))
            .then(function (data) {
                data.sort((a, b) => (a.score > b.score) ? -1 : 1)
                setPosts(data);
            }).catch(() => {
                TokenService.removeUser();
                fetchPosts(); // works?
            })
    };

    useEffect(() => {
        fetchPosts();
    }, []);

    const handleVote = (post, index, vote) => {
        var finalVote = vote;
        if (post.vote === vote) {
            finalVote = 0;
        }

        DataService.votePost(post, finalVote).then(function () {
            var postsCopy = JSON.parse(JSON.stringify(posts));
            var relativeVote = 0;
            if (post.vote === -1) {
                if (vote === -1) {
                    relativeVote = 1;
                }
                else {
                    relativeVote = 2;
                }
            } else if (post.vote === 1) {
                if (vote === -1) {
                    relativeVote = -2;
                }
                else {
                    relativeVote = -1;
                }
            } else {
                relativeVote = vote;
            }
            postsCopy[index].score = postsCopy[index].score + relativeVote;
            postsCopy[index].vote = finalVote;
            setPosts(postsCopy);
        })
    }

    const removeDeletedPost = () => {
        var postsCopy = JSON.parse(JSON.stringify(posts));
        for (var i = 0; i < postsCopy.length; i++) {
            if (postsCopy[i].id === toDelete.id) {
                postsCopy.splice(i, 1);
                setPosts(postsCopy);
                break;
            }
        }
    }

    const [confirmationOpen, setConfirmationOpen] = useState(false);
    const confirmationMessage = {
        primary: "Are you sure you want to delete the post?",
        secondary: "This will remove any associated comments"
    }

    const [alertOpen, setAlertOpen] = React.useState(false);
    const [alertSeverity, setAlertSeverity] = React.useState("info");
    const [alertMessage, setAlertMessage] = React.useState("");
    const [toDelete, setToDelete] = useState(null);

    const handleConfirmDelete = () => {
        DataService.deletePost(sub.title, toDelete.id)
            .then(() => {
                setConfirmationOpen(false);
                setAlertSeverity("info");
                setAlertMessage(`Successfully deleted post`);
                setAlertOpen(true);
                removeDeletedPost();
                setToDelete(null);
            })
            .catch((error) => {
                setAlertSeverity("error");
                setAlertMessage(`Could not delete post`);
                setAlertOpen(true);
            })
    }

    const [createOpen, setCreateOpen] = useState(false)

    return (
        <>
            <div style={{ flexGrow: 1, width: '100%' }}>
                <Grid container>
                    <Grid item xs={6} justifyContent='flex-end'>
                        <Typography variant='h5'>
                            {posts.length === 0 ? "This subreddit doesn't have any posts yet" : `${sub.title} posts:`}
                        </Typography>
                    </Grid>
                    <Grid item xs={6} style={{ display: "flex", justifyContent: 'flex-end' }}>
                        {AuthService.getCurrentUser() && <Button
                            variant="outlined"
                            color="primary"
                            style={{ maxWidth: "50%", minWidth: 100 }}
                            onClick={() => { setCreateOpen(true) }}
                        >
                            <AddIcon style={{ paddingRight: 10 }} /> Create new post
                        </Button>}
                    </Grid>
                </Grid>
            </div>
            <List>
                {
                    posts.map((post, index) => (
                        <>
                            <ListItem
                                key={post.id + "-" + post.score}
                                item={post}
                                secondaryAction={<SecondaryActionControl owner={post.creator} setConfirmationOpen={setConfirmationOpen} post={post} setToDelete={setToDelete} />}
                            >
                                <Typography style={{ paddingRight: 20 }}>
                                    {index + 1}
                                </Typography>
                                <ButtonGroup
                                    orientation="vertical"
                                    style={{ paddingRight: 20 }}
                                    variant="text"
                                >
                                    <Button
                                        key={"upvote" + post.id}
                                        disabled={AuthService.getCurrentUser() ? false : true}
                                        color={post.vote === 1 ? 'secondary' : 'primary'}
                                        style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}
                                        onClick={() => {
                                            handleVote(post, index, 1);
                                        }}
                                    >
                                        <KeyboardArrowUpIcon />
                                    </Button>
                                    <Button
                                        key={"score" + post.id}
                                        disabled={true}
                                        className='vote-button'
                                        style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}>
                                        <Typography>
                                            {post.score}
                                        </Typography>
                                    </Button>
                                    <Button
                                        key={"downvote" + post.id}
                                        disabled={AuthService.getCurrentUser() ? false : true}
                                        color={post.vote === -1 ? 'error' : 'primary'}
                                        style={{ maxWidth: '50%', maxHeight: '50%', minWidth: '50%', minHeight: '50%' }}
                                        onClick={() => {
                                            handleVote(post, index, -1);
                                        }}
                                    >
                                        <KeyboardArrowDownIcon />
                                    </Button>
                                </ButtonGroup>
                                <ListItemAvatar>
                                    <Avatar>
                                        <AccessibleIcon />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={<Link href={`${sub.title}/posts/${post.id}`}>{post.title}</Link>}
                                    secondary={post.text.length > 30 ? post.text.substring(0, 30) + "..." : post.text} />
                                <Typography variant='subtitle2'>
                                    by <strong>{post.creator_username ? post.creator_username : "[unknown]"}</strong> {HelperService.parseDate(post.date_created)} {post.last_edit ? "*(last edited " + HelperService.parseDate(post.last_edit) + ")" : ""}
                                </Typography>
                            </ListItem>

                            <Divider />
                        </>
                    ))
                }
            </List >
            <ConfirmationDialog
                open={confirmationOpen}
                setOpen={setConfirmationOpen}
                message={confirmationMessage}
                handleConfirm={handleConfirmDelete}
            />
            <PopupAlert
                severity={alertSeverity}
                message={alertMessage}
                open={alertOpen}
                setOpen={setAlertOpen}
            />
            <CreatePostDialog
                open={createOpen}
                setOpen={setCreateOpen}
                setAlertSeverity={setAlertSeverity}
                setAlertMessage={setAlertMessage}
                setAlertOpen={setAlertOpen}
                posts={posts}
                setPosts={setPosts}
                sub={sub}
            />
        </>
    )
}

function SecondaryActionControl({ owner, setConfirmationOpen, post, setToDelete }) {
    const currentUser = AuthService.getCurrentUser();

    const openConfirmationDialog = () => {
        setConfirmationOpen(true);
        setToDelete(post);
    }

    if (currentUser && (currentUser.user_id === owner || currentUser.is_admin)) {
        return (<Grid container spacing={2}>
            <Grid item xs={5}>
                <IconButton edge="end" aria-label="delete" color='error' onClick={openConfirmationDialog}>
                    <DeleteIcon />
                </IconButton>
            </Grid>
        </Grid>
        )
    }

    return (<></>)
}
