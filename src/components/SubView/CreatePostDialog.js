import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Divider } from '@mui/material';
import { useState } from 'react';
import DataService from 'services/dataService';
import ErrorFormattingService from 'services/errorFormattingService';

export default function CreatePostDialog({ setOpen, open, setAlertSeverity, setAlertMessage, setAlertOpen, posts, setPosts, sub }) {
    const handleClose = () => {
        setOpen(false);
        setShowValidationError(false);
    };

    const handleCreate = async e => {
        if (postTitle.length === 0) {
            setShowValidationError(true)
            return
        }
        DataService.createPost(sub.title, { title: postTitle, text: postText })
            .then((data) => {
                handleClose();
                setAlertSeverity("success");
                setAlertMessage("Successfully created post");
                setAlertOpen(true);

                var newRows = JSON.parse(JSON.stringify(posts));
                newRows.unshift(data.data);
                setPosts(newRows);
            })
            .catch((e) => {
                setAlertSeverity("error");
                setAlertMessage(`Could not create post: ${ErrorFormattingService.format(e)}`);
                setAlertOpen(true);
            });
    }

    const [postTitle, setPostTitle] = useState("");
    const [postText, setPostText] = useState("");
    const [showValidationError, setShowValidationError] = useState(false)

    return (
        <div>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Create new post</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To create a new sub, give it a name and a brief description
                    </DialogContentText>
                    <TextField
                        required
                        autoFocus
                        margin="dense"
                        id="title"
                        label="Post title"
                        fullWidth
                        variant="standard"
                        onChange={(e) => setPostTitle(e.target.value)}
                        error={showValidationError}
                    />
                    <Divider style={{ padding: 10 }} />
                    <TextField
                        margin="normal"
                        fullWidth
                        name="text"
                        label="Text"
                        id="text"
                        multiline
                        rows={4}
                        onChange={(e) => setPostText(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleCreate}>Create</Button>
                </DialogActions>

            </Dialog>
        </div>
    );
}
