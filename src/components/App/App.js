import { BrowserRouter, Route, Routes, Outlet } from 'react-router-dom';
import React from 'react';

import './App.css';
import Homepage from '../Homepage/Homepage';
import Login from '../Login/Login';
import Register from '../Register/Register';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import SubView from 'components/SubView/SubView';
import SubList from 'components/SubList/SubList';
import PostView from 'components/PostView/PostView';
import { Toolbar } from '@mui/material';

function App() {
  const theme = createTheme({
    palette: {
      primary: {
        light: '#33a095',
        main: '#00897b',
        dark: '#005f56',
        contrastText: '#fff',
      },
      secondary: {
        light: '#ffb199',
        main: '#ff9e80',
        dark: '#b26e59',
        contrastText: '#000',
      },
    },
  });

  const styles = {
    backgroundImage: {
      backgroundImage: "url('/background.gif')",
      backgroundPosition: '50% 35%',
      backgroundRepeat: 'no-repeat',
      backgroundSize: '20%',
    },
    overlay: {
      backgroundColor: 'rgba(255, 255, 255, 0.8)',
    }
  }

  const [partyMode, setPartyMode] = React.useState(false);
  const [friends, setFriends] = React.useState(false);

  return (
    <div style={{
      backgroundImage: partyMode ? "url('/background.gif')" : null,
      backgroundPosition: partyMode ? '50% 35%' : null,
      backgroundRepeat: friends ? 'repeat' : 'no-repeat',
      backgroundSize: '20%',
    }}>
      < div style={styles.overlay} >
        <Toolbar />
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <div className="main-body">
              <Navbar />
              <div className="wrapper">
                <Routes>
                  <Route index element={<Homepage partyMode={partyMode} setPartyMode={setPartyMode} friends={friends} setFriends={setFriends} />} />
                  <Route path="login" element={<Login />} />
                  <Route path="register" element={<Register />} />
                  <Route path="subs" element={<Outlet />}>
                    <Route index element={<SubList />} />
                    <Route path=":subName" element={<Outlet />}>
                      <Route path="posts/:postId" element={<PostView />} />
                      <Route index element={<SubView />} />
                    </Route>
                  </Route>
                </Routes>
              </div>
              <Footer />
            </div>
          </BrowserRouter>
        </ThemeProvider>
      </div >
    </div >
  );
}

export default App;
