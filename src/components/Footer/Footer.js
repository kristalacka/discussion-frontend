import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import { useTheme } from '@mui/material/styles';
import './Footer.css';

export default function Footer() {
    const theme = useTheme();

    return (
        <div className='footer-style' sx={{
            zIndex: (theme) => theme.zIndex.drawer + 1
        }}>
            <Box
                px={{ xs: 3, sm: 10 }}
                py={{ xs: 5, sm: 10 }}
                // bgcolor="text.secondary"
                bgcolor={theme.palette.primary.light}
                color="white"
            >
                <Container maxWidth="lg">
                    <Grid container spacing={5}>
                        <Grid item xs={12} sm={4}>
                            <Box borderBottom={1}>Help</Box>
                            <Box>
                                <Link href="/" color="inherit">
                                    Back to homepage
                                </Link>
                            </Box>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Box borderBottom={1}>Account</Box>
                            <Box>
                                <Link href="/login" color="inherit">
                                    Login
                                </Link>
                            </Box>
                            <Box>
                                <Link href="/register" color="inherit">
                                    Register
                                </Link>
                            </Box>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Box borderBottom={1}>Links</Box>
                            <Box>
                                <Link href="https://bitbucket.org/kristalacka/discussion-frontend/src/master/" color="inherit" target="_blank">
                                    Frontend source code
                                </Link>
                            </Box>
                            <Box>
                                <Link href="https://bitbucket.org/kristalacka/discussion/src/master/" color="inherit" target="_blank">
                                    Backend source code
                                </Link>
                            </Box>
                        </Grid>
                    </Grid>
                    <Box textAlign="center" pt={{ xs: 5, sm: 10 }} pb={{ xs: 5, sm: 0 }}>
                        Kaunas {new Date().getFullYear()}
                    </Box>
                </Container>
            </Box>
        </div>
    );
}
