import axios from "axios";
import TokenService from "./tokenService";

const instance = axios.create({
    baseURL: "http://134.122.65.173/api",
    headers: {
        "Content-Type": "application/json",
    },
});

instance.interceptors.request.use(
    (config) => {
        const token = TokenService.getLocalAccessToken();
        if (token) {
            config.headers["Authorization"] = 'Bearer ' + token;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    (res) => {
        return res;
    },
    async (err) => {
        const originalConfig = err.config;

        if (originalConfig.url !== "/token/" && err.response) {
            // Access Token was expired
            if ((err.response.status === 403 || err.response.status === 401) && !originalConfig._retry) {
                originalConfig._retry = true;

                try {
                    const rs = await instance.post("/token/refresh/", {
                        refresh: TokenService.getLocalRefreshToken(),
                    }).catch((e) => {
                        console.log(e)
                        TokenService.removeUser();
                        return Promise.reject(e);
                    });

                    const accessToken = rs.data;
                    TokenService.updateLocalAccessToken(accessToken.token);

                    return instance(originalConfig);
                } catch (_error) {
                    TokenService.removeUser();
                    return Promise.reject(_error);
                }
            }
        }

        return Promise.reject(err);
    }
);

export default instance;
