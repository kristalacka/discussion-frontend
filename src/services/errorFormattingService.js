class ErrorFormattingService {
    format(error) {
        var result = ""
        const data = error.response.data;
        for (var key in data) {
            if (Array.isArray(data[key])) {
                for (var i = 0; i < data[key].length; i++) {
                    result += data[key][i] + '\n';
                }
            }
            else {
                result += data[key];
            }
        }
        return result;
    }
}

export default new ErrorFormattingService();
