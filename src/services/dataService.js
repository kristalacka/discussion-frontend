import api from "./api";

// sub interface
const getAllSubs = () => {
    return api.get("/subs/");
}

const getSub = (id) => {
    return api.get(`/subs/${id}/`);
}

const createSub = (title, description) => {
    return api.post("/subs/", { title, description });
}

const editSub = (id, data) => {
    return api.patch(`/subs/${id}/`, data)
}

const deleteSub = (id) => {
    return api.delete(`/subs/${id}/`);
}

const subscribeSub = (id) => {
    return api.post(`/subs/${id}/subscribe/`)
}

const unsubscribeSub = (id) => {
    return api.post(`/subs/${id}/unsubscribe/`)
}

// post interface
const getAllPosts = () => {
    return api.get("/posts/");
};

const getPostsBySubs = (id) => {
    return api.get(`/subs/${id}/posts/`);
};

const getPost = (subId, id) => {
    return api.get(`/subs/${subId}/posts/${id}/`);
}

const createPost = (subId, data) => {
    return api.post(`/subs/${subId}/posts/`, data);
}

const editPost = (subId, id, data) => {
    return api.patch(`/subs/${subId}/posts/${id}/`, data);
}

const deletePost = (subId, id) => {
    return api.delete(`/subs/${subId}/posts/${id}/`);
}

const votePost = (post, vote) => {
    const body = { 'vote': vote };
    return api.post(`/subs/${post.sub_title}/posts/${post.id}/vote/`, body)
}

// comment interface
const getComments = (subId, postId) => {
    return api.get(`/subs/${subId}/posts/${postId}/comments/`);
};

const getComment = (subId, postId, id) => {
    return api.get(`/subs/${subId}/posts/${postId}/comments/${id}/`);
}

const createComment = (subId, postId, data) => {
    return api.post(`/subs/${subId}/posts/${postId}/comments/`, data);
}

const editComment = (subId, postId, id, data) => {
    return api.patch(`/subs/${subId}/posts/${postId}/comments/${id}/`, data);
}

const deleteComment = (subId, postId, id) => {
    return api.delete(`/subs/${subId}/posts/${postId}/comments/${id}/`);
}

const voteComment = (subId, postId, id, vote) => {
    const body = { 'vote': vote };
    return api.post(`/subs/${subId}/posts/${postId}/comments/${id}/vote/`, body)
}

//user interface
const getUser = (id) => {
    return api.get(`/users/${id}/`);
}

const DataService = {
    getAllSubs,
    getSub,
    createSub,
    editSub,
    deleteSub,
    subscribeSub,
    unsubscribeSub,
    getAllPosts,
    getPostsBySubs,
    getPost,
    createPost,
    editPost,
    deletePost,
    votePost,
    getComments,
    getComment,
    createComment,
    editComment,
    deleteComment,
    voteComment,
    getUser
};

export default DataService;
