import api from "./api";
import TokenService from "./tokenService";

class AuthService {
    login(username, password) {
        return api
            .post("/token/", {
                username,
                password
            })
            .then(response => {
                if (response.data.token) {
                    TokenService.setUser(response.data);
                }

                return response.data;
            });
    }

    logout() {
        TokenService.removeUser();
    }

    register(username, email, password) {
        return api.post("/register/", {
            username,
            email,
            password
        });
    }

    getCurrentUser() {
        return TokenService.getUser();
    }
}

export default new AuthService();
