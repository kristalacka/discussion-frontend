import moment from "moment";
import DataService from "./dataService";

class HelperService {
    parseDate(dateString) {
        const date = new Date(Date.parse(dateString));
        return moment(date).fromNow()
    }

    findIndex(id, collection) {
        var index = collection.findIndex(function (o) {
            return o.id === id;
        })
        return index
    }

    async fetchUsername(id) {
        return await DataService.getUser(id)
            .then((data => data.data))
            .then(function (data) {
                return data.username;
            })
            .catch((err) => {
                return "";
            })
    }
}

export default new HelperService();