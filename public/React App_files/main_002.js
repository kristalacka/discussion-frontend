webpackHotUpdate("main",{

/***/ "./src/components/PostView/PostView.js":
/*!*********************************************!*\
  !*** ./src/components/PostView/PostView.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PostView; });
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/index.js");
/* harmony import */ var services_dataService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! services/dataService */ "./src/services/dataService.js");
/* harmony import */ var services_authService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! services/authService */ "./src/services/authService.js");
/* harmony import */ var _mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @mui/icons-material/KeyboardArrowUp */ "./node_modules/@mui/icons-material/KeyboardArrowUp.js");
/* harmony import */ var _mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @mui/icons-material/KeyboardArrowDown */ "./node_modules/@mui/icons-material/KeyboardArrowDown.js");
/* harmony import */ var _mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _mui_icons_material_Accessible__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @mui/icons-material/Accessible */ "./node_modules/@mui/icons-material/Accessible.js");
/* harmony import */ var _mui_icons_material_Accessible__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Accessible__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @mui/icons-material/Delete */ "./node_modules/@mui/icons-material/Delete.js");
/* harmony import */ var _mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @mui/icons-material/Edit */ "./node_modules/@mui/icons-material/Edit.js");
/* harmony import */ var _mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _ConfirmationDialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../ConfirmationDialog */ "./src/components/ConfirmationDialog.js");
/* harmony import */ var components_PopupAlert__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! components/PopupAlert */ "./src/components/PopupAlert.js");
/* harmony import */ var _CommentList__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./CommentList */ "./src/components/PostView/CommentList.js");
/* harmony import */ var services_helperService__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! services/helperService */ "./src/services/helperService.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "C:\\Users\\Admin\\Documents\\Studijos\\saitynas\\frontend\\src\\components\\PostView\\PostView.js",
    _s = __webpack_require__.$Refresh$.signature();

















function PostView() {
  _s();

  const {
    subName,
    postId
  } = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useParams"])();

  const fetchPost = async () => {
    await services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].getPost(subName, postId).then(data => data.data).then(function (data) {
      setOriginalPost(data);
      setPost(data);
      fetchUsername(data.creator);
    }).catch(err => {});
    await services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].getComments(subName, postId).then(data => data.data).then(function (data) {
      data.sort((a, b) => a.score > b.score ? 1 : -1);
      setComments(data);
    }).catch(err => {
      console.log(err);
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetchPost();
  }, []);
  const [post, setPost] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  const [originalPost, setOriginalPost] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  const [comments, setComments] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const [editPost, setEditPost] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false); // const [error, setError] = useState(true);

  const handleChangePost = event => {
    var clone = JSON.parse(JSON.stringify(post));
    clone.text = event.target.value;
    setPost(clone);
  };

  const clickSavePost = () => {
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].editPost(subName, postId, {
      text: post.text
    }).then(() => {
      setEditPost(false);
    }).catch(e => {
      console.log(e);
    });
  };

  const clickCancelPost = () => {
    setPost(originalPost);
    setEditPost(false);
  };

  const [confirmationOpen, setConfirmationOpen] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const [confirmationMessage, setConfirmationMessage] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    primary: "Are you sure you want to delete the post?",
    secondary: "This will remove any associated comments"
  });
  const [alertOpen, setAlertOpen] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);
  const [alertSeverity, setAlertSeverity] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState("info");
  const [alertMessage, setAlertMessage] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState("");
  const navigate = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useNavigate"])();

  const handleConfirmDelete = () => {
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].deletePost(subName, postId).then(() => {
      setConfirmationOpen(false);
      navigate(`/subs/${subName}`);
    }).catch(error => {
      setAlertSeverity("error");
      setAlertMessage(`Could not delete post`);
      setAlertOpen(true);
    });
  };

  const [username, setUsername] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");

  const fetchUsername = id => {
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].getUser(id).then(data => data.data).then(function (data) {
      setUsername(data.username);
    }).catch(err => {
      console.log(err);
    });
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])("div", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(PostHeader, {
      subName: subName,
      post: post,
      setPost: setPost,
      setEditPost: setEditPost,
      setConfirmationOpen: setConfirmationOpen
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 110,
      columnNumber: 13
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Divider"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 117,
      columnNumber: 13
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Typography"], {
      variant: "body1",
      children: editPost ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["TextField"], {
        fullWidth: true,
        disabled: !editPost,
        id: "standard-multiline-flexible",
        multiline: true,
        value: post ? post.text : "",
        onChange: handleChangePost,
        variant: "standard",
        style: {
          padding: 20,
          backgroundColor: 'rgb(250,250,250)'
        }
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 119,
        columnNumber: 29
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Box"], {
          style: {
            padding: 20,
            backgroundColor: 'rgb(250,250,250)'
          },
          children: post ? post.text : ""
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 131,
          columnNumber: 25
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Divider"], {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 134,
          columnNumber: 25
        }, this)]
      }, void 0, true)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 13
    }, this), editPost && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["Fragment"], {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
        onClick: () => clickCancelPost(),
        children: "Cancel"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 141,
        columnNumber: 21
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
        onClick: () => clickSavePost(),
        children: "Save"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 144,
        columnNumber: 21
      }, this)]
    }, void 0, true), post && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Typography"], {
      variant: "subtitle2",
      children: ["by ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])("strong", {
        children: username
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 151,
        columnNumber: 24
      }, this), " posted ", services_helperService__WEBPACK_IMPORTED_MODULE_13__["default"].parseDate(post.date_created), " ", post.last_edit ? "*(last edited " + services_helperService__WEBPACK_IMPORTED_MODULE_13__["default"].parseDate(post.last_edit) + ")" : ""]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 150,
      columnNumber: 17
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Divider"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 154,
      columnNumber: 13
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_CommentList__WEBPACK_IMPORTED_MODULE_12__["default"], {
      comments: comments,
      setComments: setComments,
      post: post,
      subName: subName
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 13
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_ConfirmationDialog__WEBPACK_IMPORTED_MODULE_10__["default"], {
      open: confirmationOpen,
      setOpen: setConfirmationOpen,
      message: confirmationMessage,
      handleConfirm: handleConfirmDelete
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 161,
      columnNumber: 13
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(components_PopupAlert__WEBPACK_IMPORTED_MODULE_11__["default"], {
      severity: alertSeverity,
      message: alertMessage,
      open: alertOpen,
      setOpen: setAlertOpen
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 167,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 109,
    columnNumber: 9
  }, this);
}

_s(PostView, "tfUGEVGGJ2welzDRkKRlTIS/HcQ=", false, function () {
  return [react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useParams"], react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useNavigate"]];
});

_c = PostView;

function PostHeader(_ref) {
  let {
    subName,
    post,
    setPost,
    setEditPost,
    setConfirmationOpen
  } = _ref;

  const handleVote = (post, vote) => {
    var finalVote = vote;

    if (post.vote === vote) {
      finalVote = 0;
    }

    var relativeVote = 0;
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].votePost(post, finalVote).then(function () {
      if (post.vote === -1) {
        if (vote === -1) {
          relativeVote = 1;
        } else {
          relativeVote = 2;
        }
      } else if (post.vote === 1) {
        if (vote === -1) {
          relativeVote = -2;
        } else {
          relativeVote = -1;
        }
      } else {
        relativeVote = vote;
      }
    }).then(() => {
      var postCopy = JSON.parse(JSON.stringify(post));
      postCopy.score = postCopy.score + relativeVote;
      postCopy.vote = finalVote;
      setPost(postCopy);
    });
  };

  if (!post) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])("div", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 213,
      columnNumber: 17
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["List"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["ListItem"], {
        secondaryAction: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(SecondaryActionControl, {
          owner: post.creator,
          setEditPost: setEditPost,
          setConfirmationOpen: setConfirmationOpen
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 219,
          columnNumber: 44
        }, this),
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["ButtonGroup"], {
          orientation: "vertical",
          style: {
            paddingRight: 20
          },
          variant: "text",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
            disabled: services_authService__WEBPACK_IMPORTED_MODULE_4__["default"].getCurrentUser() ? false : true,
            color: post.vote === 1 ? 'secondary' : 'primary',
            style: {
              maxWidth: '50%',
              maxHeight: '50%',
              minWidth: '50%',
              minHeight: '50%'
            },
            onClick: () => {
              handleVote(post, 1);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_5___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 239,
              columnNumber: 29
            }, this)
          }, "upvote" + post.id, false, {
            fileName: _jsxFileName,
            lineNumber: 230,
            columnNumber: 25
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
            disabled: true,
            className: "vote-button",
            style: {
              maxWidth: '50%',
              maxHeight: '50%',
              minWidth: '50%',
              minHeight: '50%'
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Typography"], {
              children: post.score
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 246,
              columnNumber: 29
            }, this)
          }, "score" + post.id, false, {
            fileName: _jsxFileName,
            lineNumber: 241,
            columnNumber: 25
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
            disabled: services_authService__WEBPACK_IMPORTED_MODULE_4__["default"].getCurrentUser() ? false : true,
            color: post.vote === -1 ? 'error' : 'primary',
            style: {
              maxWidth: '50%',
              maxHeight: '50%',
              minWidth: '50%',
              minHeight: '50%'
            },
            onClick: () => {
              handleVote(post, -1);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_6___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 259,
              columnNumber: 29
            }, this)
          }, "downvote" + post.id, false, {
            fileName: _jsxFileName,
            lineNumber: 250,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 225,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["ListItemAvatar"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Avatar"], {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_icons_material_Accessible__WEBPACK_IMPORTED_MODULE_7___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 264,
              columnNumber: 29
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 263,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 262,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["ListItemText"], {
          primary: post.title
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 267,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 219,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 218,
      columnNumber: 13
    }, this)
  }, void 0, false);
}

_c2 = PostHeader;

function SecondaryActionControl(_ref2) {
  let {
    owner,
    setEditPost,
    setConfirmationOpen
  } = _ref2;
  const currentUser = services_authService__WEBPACK_IMPORTED_MODULE_4__["default"].getCurrentUser();

  const openConfirmationDialog = () => {
    setConfirmationOpen(true);
  };

  if (currentUser && currentUser.user_id === owner) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
      container: true,
      spacing: 2,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
        item: true,
        xs: 5,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["IconButton"], {
          edge: "end",
          "aria-label": "edit",
          color: "primary",
          onClick: () => {
            setEditPost(true);
          },
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_9___default.a, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 285,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 284,
          columnNumber: 17
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 283,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
        item: true,
        xs: 5,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["IconButton"], {
          edge: "end",
          "aria-label": "delete",
          color: "error",
          onClick: openConfirmationDialog,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_8___default.a, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 290,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 289,
          columnNumber: 17
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 288,
        columnNumber: 13
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 282,
      columnNumber: 17
    }, this);
  } else if (currentUser && currentUser.is_admin) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
      container: true,
      spacing: 2,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
        item: true,
        xs: 5,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["IconButton"], {
          edge: "end",
          "aria-label": "delete",
          color: "error",
          onClick: openConfirmationDialog,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(_mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_8___default.a, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 301,
            columnNumber: 25
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 300,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 299,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 298,
      columnNumber: 13
    }, this);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_14__["Fragment"], {}, void 0, false);
}

_c3 = SecondaryActionControl;

var _c, _c2, _c3;

__webpack_require__.$Refresh$.register(_c, "PostView");
__webpack_require__.$Refresh$.register(_c2, "PostHeader");
__webpack_require__.$Refresh$.register(_c3, "SecondaryActionControl");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ })

})
//# sourceMappingURL=main.93f4694010ff10de61f2.hot-update.js.map