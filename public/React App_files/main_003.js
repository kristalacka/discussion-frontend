webpackHotUpdate("main",{

/***/ "./src/components/PostView/Comment.js":
/*!********************************************!*\
  !*** ./src/components/PostView/Comment.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Comment; });
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mui/material */ "./node_modules/@mui/material/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var services_authService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! services/authService */ "./src/services/authService.js");
/* harmony import */ var services_dataService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! services/dataService */ "./src/services/dataService.js");
/* harmony import */ var _mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @mui/icons-material/KeyboardArrowUp */ "./node_modules/@mui/icons-material/KeyboardArrowUp.js");
/* harmony import */ var _mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @mui/icons-material/KeyboardArrowDown */ "./node_modules/@mui/icons-material/KeyboardArrowDown.js");
/* harmony import */ var _mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var services_helperService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! services/helperService */ "./src/services/helperService.js");
/* harmony import */ var _mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @mui/icons-material/Delete */ "./node_modules/@mui/icons-material/Delete.js");
/* harmony import */ var _mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @mui/icons-material/Edit */ "./node_modules/@mui/icons-material/Edit.js");
/* harmony import */ var _mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var components_ConfirmationDialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! components/ConfirmationDialog */ "./src/components/ConfirmationDialog.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "C:\\Users\\Admin\\Documents\\Studijos\\saitynas\\frontend\\src\\components\\PostView\\Comment.js",
    _s = __webpack_require__.$Refresh$.signature();













function Comment(_ref) {
  _s();

  let {
    comment,
    level,
    subName,
    post,
    comments,
    setComments
  } = _ref;
  const [username, setUsername] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");

  const fetchUsername = id => {
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].getUser(id).then(data => data.data).then(function (data) {
      setUsername(data.username);
    }).catch(err => {
      console.log(err);
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetchUsername(comment.creator);
  }, []);

  const handleVote = vote => {
    var finalVote = vote;

    if (comment.vote === vote) {
      finalVote = 0;
    }

    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].voteComment(subName, post.id, comment.id, finalVote).then(function () {
      var relativeVote = 0;

      if (comment.vote === -1) {
        if (vote === -1) {
          relativeVote = 1;
        } else {
          relativeVote = 2;
        }
      } else if (comment.vote === 1) {
        if (vote === -1) {
          relativeVote = -2;
        } else {
          relativeVote = -1;
        }
      } else {
        relativeVote = vote;
      }

      var commentsCopy = JSON.parse(JSON.stringify(comments));
      comment.score += relativeVote;
      comment.vote = finalVote;
      commentsCopy = updateComment(commentsCopy, comment);
      setComments(commentsCopy);
    });
  };

  const [replyOpen, setReplyOpen] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const [insertComment, setInsertComment] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  const [commentBlank, setCommentBlank] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);

  const makeSpacers = () => {
    var items = [];

    for (let index = 0; index < level; index++) {
      items.push( /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
        item: true,
        xs: 1,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Divider"], {
          orientation: "vertical"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 21
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 17
      }, this));
    }

    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
      children: items
    }, void 0, false);
  };

  const insertChildComment = (commentList, insertedComment) => {
    for (var i = 0; i < commentList.length; i++) {
      if (commentList[i].id === comment.id) {
        commentList[i].children.unshift(insertedComment);
        return commentList;
      }

      insertChildComment(commentList[i].children, insertedComment);
    }

    return commentList;
  };

  const updateComment = (commentList, newComment) => {
    for (var i = 0; i < commentList.length; i++) {
      if (commentList[i].id === comment.id) {
        commentList[i] = newComment;
        return commentList;
      }

      updateComment(commentList[i].children, newComment);
    }

    return commentList;
  };

  const clickSaveComment = () => {
    if (insertComment.length === 0) {
      setCommentBlank(true);
      return;
    }

    setCommentBlank(false);
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].createComment(subName, post.id, {
      text: insertComment,
      parent: comment.id
    }).then(response => {
      setReplyOpen(false);
      setInsertComment("");
      var commentsCopy = JSON.parse(JSON.stringify(comments));
      commentsCopy = insertChildComment(commentsCopy, response.data);
      setComments(commentsCopy);
    });
  };

  const [editComment, setEditComment] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const [confirmationOpen, setConfirmationOpen] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const confirmationMessage = {
    primary: "Are you sure you want to delete this comment?",
    secondary: "The replies will be unaffected"
  };

  const handleConfirmDelete = () => {
    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].editComment(subName, post.id, comment.id, {
      text: null
    }).then(() => {
      comment.text = null;
      comment.creator = null;
      var commentsCopy = JSON.parse(JSON.stringify(comments));
      updateComment(commentsCopy, comment);
      setComments(commentsCopy);
      setConfirmationOpen(false);
    });
  };

  const [editCommentValue, setEditCommentValue] = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");

  const clickEditComment = () => {
    if (editCommentValue === comment.text) {
      setEditComment(false);
      return;
    }

    services_dataService__WEBPACK_IMPORTED_MODULE_3__["default"].editComment(subName, post.id, comment.id, {
      text: editCommentValue
    }).then(() => {
      comment.text = editCommentValue;
      var currentdate = new Date();
      comment.last_edited = currentdate.toLocaleString('en-US');
      var commentsCopy = JSON.parse(JSON.stringify(comments));
      updateComment(commentsCopy, comment);
      setComments(commentsCopy);
      setEditComment(false);
    });
  };

  const currentUser = services_authService__WEBPACK_IMPORTED_MODULE_2__["default"].getCurrentUser();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
    children: [level > 0 && makeSpacers(), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Grid"], {
      item: true,
      xs: level > 6 ? 6 : 12 - level,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["ListItem"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["ButtonGroup"], {
          orientation: "vertical",
          variant: "text",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
            disabled: currentUser ? false : true,
            color: comment.vote === 1 ? 'secondary' : 'primary',
            style: {
              maxWidth: '30%',
              maxHeight: '30%',
              minWidth: '30%',
              minHeight: '30%'
            },
            onClick: () => {
              handleVote(1);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_icons_material_KeyboardArrowUp__WEBPACK_IMPORTED_MODULE_4___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 180,
              columnNumber: 29
            }, this)
          }, "upvote" + comment.id, false, {
            fileName: _jsxFileName,
            lineNumber: 171,
            columnNumber: 25
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
            disabled: currentUser ? false : true,
            color: comment.vote === -1 ? 'error' : 'primary',
            style: {
              maxWidth: '30%',
              maxHeight: '30%',
              minWidth: '30%',
              minHeight: '30%'
            },
            onClick: () => {
              handleVote(-1);
            },
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_icons_material_KeyboardArrowDown__WEBPACK_IMPORTED_MODULE_5___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 191,
              columnNumber: 29
            }, this)
          }, "downvote" + comment.id, false, {
            fileName: _jsxFileName,
            lineNumber: 182,
            columnNumber: 25
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 167,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Box"], {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Typography"], {
            variant: "subtitle2",
            children: ["by ", comment.text ? username : "[deleted]", " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])("strong", {
              children: [comment.score, " points"]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 196,
              columnNumber: 72
            }, this), " ", services_helperService__WEBPACK_IMPORTED_MODULE_6__["default"].parseDate(comment.date_created), " ", comment.last_edit ? "*(last edited " + services_helperService__WEBPACK_IMPORTED_MODULE_6__["default"].parseDate(comment.last_edit) + ")" : ""]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 195,
            columnNumber: 25
          }, this), editComment ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["TextField"], {
              fullWidth: true,
              id: "standard-multiline-flexible",
              multiline: true,
              defaultValue: comment.text,
              onChange: event => {
                setEditCommentValue(event.target.value);
              },
              variant: "standard",
              error: commentBlank
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 200,
              columnNumber: 33
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
              onClick: () => setEditComment(false),
              children: "Cancel"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 209,
              columnNumber: 33
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
              onClick: () => clickEditComment(),
              children: "Save"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 212,
              columnNumber: 33
            }, this)]
          }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Typography"], {
            variant: "body1",
            children: comment.text ? comment.text : "[deleted]"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 216,
            columnNumber: 29
          }, this), currentUser && !editComment && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Typography"], {
            variant: "subtitle2",
            paddingTop: 1,
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Box"], {
              sx: {
                display: 'inline'
              },
              paddingRight: 2,
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Link"], {
                component: "button",
                onClick: () => {
                  setReplyOpen(true);
                },
                children: "reply"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 224,
                columnNumber: 37
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 223,
              columnNumber: 33
            }, this), comment.text && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
              children: currentUser.user_id === comment.creator ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
                  color: "primary",
                  startIcon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_icons_material_Edit__WEBPACK_IMPORTED_MODULE_8___default.a, {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 232,
                    columnNumber: 84
                  }, this),
                  onClick: () => {
                    setEditComment(true);
                  },
                  style: {
                    textTransform: 'none',
                    fontSize: 'small'
                  },
                  children: "edit"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 232,
                  columnNumber: 49
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
                  color: "error",
                  startIcon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_7___default.a, {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 235,
                    columnNumber: 82
                  }, this),
                  onClick: () => {
                    setConfirmationOpen(true);
                  },
                  style: {
                    textTransform: 'none',
                    fontSize: 'small'
                  },
                  children: "delete"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 235,
                  columnNumber: 49
                }, this)]
              }, void 0, true) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
                  color: "error",
                  startIcon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_icons_material_Delete__WEBPACK_IMPORTED_MODULE_7___default.a, {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 242,
                    columnNumber: 82
                  }, this),
                  onClick: () => {
                    setConfirmationOpen(true);
                  },
                  style: {
                    textTransform: 'none',
                    fontSize: 'small'
                  },
                  children: "delete"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 242,
                  columnNumber: 49
                }, this)
              }, void 0, false)
            }, void 0, false)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 222,
            columnNumber: 29
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 194,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 166,
        columnNumber: 17
      }, this), replyOpen && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["Fragment"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["TextField"], {
          fullWidth: true,
          id: "standard-multiline-flexible",
          multiline: true,
          placeholder: "Write a comment",
          value: currentUser ? insertComment : "Log in or sign up to comment",
          onChange: event => {
            setInsertComment(event.target.value);
          },
          variant: "standard",
          error: commentBlank
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 257,
          columnNumber: 25
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
          onClick: () => setReplyOpen(false),
          children: "Cancel"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 267,
          columnNumber: 25
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(_mui_material__WEBPACK_IMPORTED_MODULE_0__["Button"], {
          onClick: () => clickSaveComment(),
          children: "Save"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 270,
          columnNumber: 25
        }, this)]
      }, void 0, true)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 165,
      columnNumber: 13
    }, this), comment.children.map((child, index) => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(Comment, {
      comment: child,
      level: level + 1,
      subName: subName,
      post: post,
      comments: comments,
      setComments: setComments
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 278,
      columnNumber: 21
    }, this)), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__["jsxDEV"])(components_ConfirmationDialog__WEBPACK_IMPORTED_MODULE_9__["default"], {
      open: confirmationOpen,
      setOpen: setConfirmationOpen,
      message: confirmationMessage,
      handleConfirm: handleConfirmDelete
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 281,
      columnNumber: 13
    }, this)]
  }, void 0, true);
}

_s(Comment, "LbwxcLdobsKJPizSjMpGRS8FJ+o=");

_c = Comment;

var _c;

__webpack_require__.$Refresh$.register(_c, "Comment");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ })

})
//# sourceMappingURL=main.65d90739f8ba7b7fcecf.hot-update.js.map